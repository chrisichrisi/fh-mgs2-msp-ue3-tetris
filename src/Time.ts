let DT = 0;
let Last_Absolute = 0;
let Now_Absolute = 0;

export function Update(timestamp: number) {
  Last_Absolute = Now_Absolute;
  Now_Absolute = timestamp;
  DT = (Now_Absolute - Last_Absolute) / 1000;
  if (DT > 1) {
    DT = 1;
  } // Fix for debugging

  return { DT, Last_Absolute, Now_Absolute };
}
