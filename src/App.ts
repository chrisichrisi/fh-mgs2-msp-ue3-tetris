/**
 * Author:  Christoph Kappler
 * Date:    2014-04-09
 * File:    Tetris.js
 *
 * Purpose: This is the tetris clone in javascript with a little bit jQuery.
 *
 */

import $ from "jquery";

import { RandomInt } from "./util";
import { Keys, Input } from "./input";
import { Update } from "./Time";
import { GameStates } from "./GameStates";

interface Vector2D {
  x: number;
  y: number;
}

const ShapeTypes = {
  CLEAR_RECT: -1,
  NO_SHAPE: 0,
  T: 10,
  J: 11,
  Z: 12,
  O: 13,
  S: 14,
  L: 15,
  I: 16,

  colors: new Array(),
  rotations: new Array(),
  nextRotation: new Array(),
  stones: new Array(),
  noCheckNeighbours: new Array(),
  Init: function() {
    // Setup colors
    this.colors[this.NO_SHAPE] = "#FFFFFF";
    this.colors[this.T] = "#FF0000";
    this.colors[this.J] = "#FFDB00";
    this.colors[this.Z] = "#49FF00";
    this.colors[this.O] = "#00FF92";
    this.colors[this.S] = "#0092FF";
    this.colors[this.L] = "#4900FF";
    this.colors[this.I] = "#FF00DB";

    // Setup rotations
    this.rotations[this.T] = new Array("Tu", "Tr", "Td", "Tl");
    this.nextRotation[this.T] = new Array();
    this.nextRotation[this.T]["Tu"] = "Tr";
    this.nextRotation[this.T]["Tr"] = "Td";
    this.nextRotation[this.T]["Td"] = "Tl";
    this.nextRotation[this.T]["Tl"] = "Tu";
    this.rotations[this.J] = new Array("Jl", "Ju", "Jr", "Jd");
    this.nextRotation[this.J] = new Array();
    this.nextRotation[this.J]["Jl"] = "Ju";
    this.nextRotation[this.J]["Ju"] = "Jr";
    this.nextRotation[this.J]["Jr"] = "Jd";
    this.nextRotation[this.J]["Jd"] = "Jl";
    this.rotations[this.Z] = new Array("Zh", "Zv");
    this.nextRotation[this.Z] = new Array();
    this.nextRotation[this.Z]["Zh"] = "Zv";
    this.nextRotation[this.Z]["Zv"] = "Zh";
    this.rotations[this.O] = new Array("O");
    this.nextRotation[this.O] = new Array();
    this.nextRotation[this.O]["O"] = "O";
    this.rotations[this.S] = new Array("Sh", "Sv");
    this.nextRotation[this.S] = new Array();
    this.nextRotation[this.S]["Sh"] = "Sv";
    this.nextRotation[this.S]["Sv"] = "Sh";
    this.rotations[this.L] = new Array("Lr", "Ld", "Ll", "Lu");
    this.nextRotation[this.L] = new Array();
    this.nextRotation[this.L]["Lr"] = "Ld";
    this.nextRotation[this.L]["Ld"] = "Ll";
    this.nextRotation[this.L]["Ll"] = "Lu";
    this.nextRotation[this.L]["Lu"] = "Lr";
    this.rotations[this.I] = new Array("Iv", "Ih");
    this.nextRotation[this.I] = new Array();
    this.nextRotation[this.I]["Iv"] = "Ih";
    this.nextRotation[this.I]["Ih"] = "Iv";

    // Setup stones
    this.stones[this.T] = new Array();
    this.SetupStone(
      this.T,
      "Tu",
      { x: 0, y: -1 },
      { x: -1, y: 0 },
      { x: 1, y: 0 }
    );
    this.SetupStone(
      this.T,
      "Tr",
      { x: 1, y: 0 },
      { x: 0, y: -1 },
      { x: 0, y: 1 }
    );
    this.SetupStone(
      this.T,
      "Td",
      { x: 0, y: 1 },
      { x: 1, y: 0 },
      { x: -1, y: 0 }
    );
    this.SetupStone(
      this.T,
      "Tl",
      { x: -1, y: 0 },
      { x: 0, y: 1 },
      { x: 0, y: -1 }
    );

    this.stones[this.J] = new Array();
    this.SetupStone(
      this.J,
      "Jl",
      { x: -1, y: 1 },
      { x: 0, y: 1 },
      { x: 0, y: -1 }
    );
    this.SetupStone(
      this.J,
      "Ju",
      { x: -1, y: -1 },
      { x: -1, y: 0 },
      { x: 1, y: 0 }
    );
    this.SetupStone(
      this.J,
      "Jr",
      { x: 1, y: -1 },
      { x: 0, y: -1 },
      { x: 0, y: 1 }
    );
    this.SetupStone(
      this.J,
      "Jd",
      { x: 1, y: 1 },
      { x: 1, y: 0 },
      { x: -1, y: 0 }
    );

    this.stones[this.Z] = new Array();
    this.SetupStone(
      this.Z,
      "Zh",
      { x: -1, y: 0 },
      { x: 0, y: 1 },
      { x: 1, y: 1 }
    );
    this.SetupStone(
      this.Z,
      "Zv",
      { x: 0, y: 1 },
      { x: 1, y: 0 },
      { x: 1, y: -1 }
    );

    this.stones[this.O] = new Array();
    this.SetupStone(
      this.O,
      "O",
      { x: -1, y: 0 },
      { x: -1, y: 1 },
      { x: 0, y: 1 }
    );

    this.stones[this.S] = new Array();
    this.SetupStone(
      this.S,
      "Sh",
      { x: 1, y: 0 },
      { x: 0, y: 1 },
      { x: -1, y: 1 }
    );
    this.SetupStone(
      this.S,
      "Sv",
      { x: 0, y: 1 },
      { x: -1, y: 0 },
      { x: -1, y: -1 }
    );

    this.stones[this.L] = new Array();
    this.SetupStone(
      this.L,
      "Lr",
      { x: 1, y: 1 },
      { x: 0, y: 1 },
      { x: 0, y: -1 }
    );
    this.SetupStone(
      this.L,
      "Ld",
      { x: -1, y: 1 },
      { x: -1, y: 0 },
      { x: 1, y: 0 }
    );
    this.SetupStone(
      this.L,
      "Ll",
      { x: -1, y: -1 },
      { x: 0, y: -1 },
      { x: 0, y: 1 }
    );
    this.SetupStone(
      this.L,
      "Lu",
      { x: 1, y: -1 },
      { x: 1, y: 0 },
      { x: -1, y: 0 }
    );

    this.stones[this.I] = new Array();
    this.SetupStone(
      this.I,
      "Iv",
      { x: 0, y: 1 },
      { x: 0, y: -1 },
      { x: 0, y: -2 }
    );
    this.SetupStone(
      this.I,
      "Ih",
      { x: 1, y: 0 },
      { x: -1, y: 0 },
      { x: -2, y: 0 }
    );

    // Setup NoCheckNeighbours
    this.noCheckNeighbours[this.T] = new Array();
    this.noCheckNeighbours[this.T]["Tu"] = ["N0"];
    this.noCheckNeighbours[this.T]["Tr"] = ["N1", "Or"];
    this.noCheckNeighbours[this.T]["Td"] = ["Or"];
    this.noCheckNeighbours[this.T]["Tl"] = ["N2", "Or"];

    this.noCheckNeighbours[this.J] = new Array();
    this.noCheckNeighbours[this.J]["Jl"] = ["N2", "Or"];
    this.noCheckNeighbours[this.J]["Ju"] = ["N0"];
    this.noCheckNeighbours[this.J]["Jr"] = ["N1", "Or"];
    this.noCheckNeighbours[this.J]["Jd"] = ["N1"];

    this.noCheckNeighbours[this.Z] = new Array();
    this.noCheckNeighbours[this.Z]["Zh"] = ["Or"];
    this.noCheckNeighbours[this.Z]["Zv"] = ["N2"];

    this.noCheckNeighbours[this.O] = new Array();
    this.noCheckNeighbours[this.O]["O"] = ["N0", "Or"];

    this.noCheckNeighbours[this.S] = new Array();
    this.noCheckNeighbours[this.S]["Sh"] = ["Or"];
    this.noCheckNeighbours[this.S]["Sv"] = ["N2"];

    this.noCheckNeighbours[this.L] = new Array();
    this.noCheckNeighbours[this.L]["Lr"] = ["N2", "Or"];
    this.noCheckNeighbours[this.L]["Ld"] = ["N1"];
    this.noCheckNeighbours[this.L]["Ll"] = ["N1", "Or"];
    this.noCheckNeighbours[this.L]["Lu"] = ["N0"];

    this.noCheckNeighbours[this.I] = new Array();
    this.noCheckNeighbours[this.I]["Iv"] = ["N2", "N1", "Or"];
    this.noCheckNeighbours[this.I]["Ih"] = [];
  },

  SetupStone: function(type, rot, n0, n1, n2) {
    this.stones[type][rot] = new Array();
    this.stones[type][rot]["Or"] = { x: 0, y: 0 };
    this.stones[type][rot]["N0"] = n0;
    this.stones[type][rot]["N1"] = n1;
    this.stones[type][rot]["N2"] = n2;
  },
  GetRotation: function(type, rotation) {
    if (rotation == 0) {
      // Default rotation
      return this.rotations[type][0];
    }
    return this.nextRotation[type][rotation];
  },

  GetStones: function(type, rotation) {
    return this.stones[type][rotation];
  },
  GetNoCheckNeighbours: function(type, rotation) {
    return this.noCheckNeighbours[type][rotation];
  },
  GetColor: function(type) {
    return this.colors[type];
  },
  GetCSSClass: function(type) {
    if (type == this.T) {
      return "nstet-t";
    }
    if (type == this.J) {
      return "nstet-j";
    }
    if (type == this.Z) {
      return "nstet-z";
    }
    if (type == this.O) {
      return "nstet-o";
    }
    if (type == this.S) {
      return "nstet-s";
    }
    if (type == this.L) {
      return "nstet-l";
    }
    if (type == this.I) {
      return "nstet-i";
    }
  }
};
ShapeTypes.Init();

const Tetris = {
  State: GameStates.Running,

  Init: function() {
    canvas = $("#canvas")[0];
    context = canvas.getContext("2d");

    GlobalInfo.CANVAS_WIDTH = GlobalInfo.BLOCK_WIDTH * Grid.COLS;
    GlobalInfo.CANVAS_HEIGHT = GlobalInfo.BLOCK_HEIGHT * Grid.ROWS;
    $(canvas).width(GlobalInfo.CANVAS_WIDTH);
    $(canvas).height(GlobalInfo.CANVAS_HEIGHT);
  },

  Start: function() {
    Grid.Init();
    this.Resume();
  },
  Pause: function() {
    this.State = GameStates.Paused;
  },
  Resume: function() {
    this.State = GameStates.Running;
    Grid.redrawRow = Grid.ROWS - 1;
    requestAnimationFrame(Run);
  },
  Ended: function() {
    this.State = GameStates.Ended;
  }
};

let canvas;
let context;
let nextStone;

const GlobalInfo = {
  CANVAS_WIDTH: 0,
  CANVAS_HEIGHT: 0,

  BLOCK_WIDTH: 30,
  BLOCK_HEIGHT: 30,

  SHADOW_SIZE: 3,
  SHADOW_COLOR: "#FFF"
};

const Grid = {
  // Members
  ROWS: 20,
  COLS: 10,
  grid: new Array(),

  moved: false, // true, if a block has moved
  spawn_new: true, // true, if a new block should be spawned
  redrawRow: -1, // Defines the row where all rows above need to be redrawn
  fullRows: new Array(),

  /* Members for move*/
  Moving: {
    MOVE_TIME: 0.1,
    MOVE_DOWN_TIME: 0.7,
    timerMove: 0,
    timerMoveDown: 0
  },
  /* Members for blinking animation */
  Blinking: {
    BLINK_TIME: 0.4,
    BLINK_COUNT: 5,
    timer: 0.4,
    counter: 0,
    isBlinking: false,
    isVisible: false
  },

  /* Member for current tetrimino*/
  CURRENT: {
    pos: { x: 0, y: 0 },
    nextType: -1,
    type: 0,
    drawType: 0,
    rotation: 0,

    stones: null,
    stonesFull: new Array(),
    noCheckNeighbours: null,

    // Functions
    CanMakeNewMove: function(poses, noCheckNeighbours) {
      for (let index in poses) {
        //if ('undefined' == typeof noCheckNeighbours[index]) {
        if (!(index in noCheckNeighbours)) {
          if (!Grid.CheckInBounds(poses[index])) {
            return false;
          }
          if (!Grid.CheckIsEmptyCell(poses[index])) {
            return false;
          }
        }
      }
      return true;
    },
    New: function() {
      if (this.nextType == -1) {
        this.nextType = RandomInt(ShapeTypes.T, ShapeTypes.I);
      }
      this.type = this.nextType;
      this.nextType = RandomInt(ShapeTypes.T, ShapeTypes.I);
      // Update next stone preview
      nextStone.attr(
        "class",
        "nextStone " + ShapeTypes.GetCSSClass(this.nextType)
      );

      this.drawType = this.type + 10;
      this.pos.x = Math.ceil(Grid.COLS / 2);
      this.pos.y = 0;

      this.rotation = ShapeTypes.GetRotation(this.type, 0);
      this.stones = ShapeTypes.GetStones(this.type, this.rotation);
      this.noCheckNeighbours = ShapeTypes.GetNoCheckNeighbours(
        this.type,
        this.rotation
      );

      var poses = this.GetNewFullStonePos(this.pos);

      if (!this.CanMakeNewMove(poses, this.noCheckNeighbours)) {
        return false;
      }
      this.Update();
      return true;
    },
    MoveDown: function() {
      var newPos = { x: this.pos.x, y: this.pos.y + 1 };
      var poses = this.GetNewFullStonePos(newPos);

      if (!this.CanMakeNewMove(poses, this.noCheckNeighbours)) {
        return false;
      }

      this.Update(newPos);
      return true;
    },
    Rotate: function() {
      var newRotation = ShapeTypes.GetRotation(this.type, this.rotation);
      var newStones = ShapeTypes.GetStones(this.type, newRotation);
      var newNCN = ShapeTypes.GetNoCheckNeighbours(this.type, newRotation);

      var poses = this.GetNewFullStonePos(this.pos, newStones);

      if (!this.CanMakeNewMove(poses, newNCN)) {
        return false;
      }
      this.rotation = newRotation;
      this.stones = newStones;
      this.noCheckNeighbours = newNCN;

      this.Update(this.pos);
    },
    Move: function() {
      Grid.grid[this.pos.y][this.pos.x] = ShapeTypes.NO_SHAPE;
      var newPos = { x: this.pos.x, y: this.pos.y };
      if (Input.IsDown(Keys.LEFT)) {
        newPos.x--;
      }
      if (Input.IsDown(Keys.RIGHT)) {
        newPos.x++;
      }

      var poses = this.GetNewFullStonePos(newPos);
      if (!this.CanMakeNewMove(poses, this.noCheckNeighbours)) {
        return false;
      }
      this.Update(newPos);
    },
    Update: function(newPos) {
      if (newPos == undefined) {
        newPos = this.pos;
      } else {
        Grid.grid[this.stonesFull["Or"].y][this.stonesFull["Or"].x] =
          ShapeTypes.CLEAR_RECT;
        Grid.grid[this.stonesFull["N0"].y][this.stonesFull["N0"].x] =
          ShapeTypes.CLEAR_RECT;
        Grid.grid[this.stonesFull["N1"].y][this.stonesFull["N1"].x] =
          ShapeTypes.CLEAR_RECT;
        Grid.grid[this.stonesFull["N2"].y][this.stonesFull["N2"].x] =
          ShapeTypes.CLEAR_RECT;
      }

      this.SetNewPos(newPos);
      Grid.grid[this.stonesFull["Or"].y][
        this.stonesFull["Or"].x
      ] = this.drawType;
      Grid.grid[this.stonesFull["N0"].y][
        this.stonesFull["N0"].x
      ] = this.drawType;
      Grid.grid[this.stonesFull["N1"].y][
        this.stonesFull["N1"].x
      ] = this.drawType;
      Grid.grid[this.stonesFull["N2"].y][
        this.stonesFull["N2"].x
      ] = this.drawType;
    },
    FixiateStones: function() {
      Grid.grid[this.stonesFull["Or"].y][this.stonesFull["Or"].x] = this.type;
      Grid.grid[this.stonesFull["N0"].y][this.stonesFull["N0"].x] = this.type;
      Grid.grid[this.stonesFull["N1"].y][this.stonesFull["N1"].x] = this.type;
      Grid.grid[this.stonesFull["N2"].y][this.stonesFull["N2"].x] = this.type;
    },
    GetNewFullStonePos: function(newPos, stones) {
      if (typeof stones === "undefined") {
        stones = this.stones;
      }
      var stonePositions = new Array();
      stonePositions["Or"] = {
        x: newPos.x + stones["Or"].x,
        y: newPos.y + stones["Or"].y
      };
      stonePositions["N0"] = {
        x: newPos.x + stones["N0"].x,
        y: newPos.y + stones["N0"].y
      };
      stonePositions["N1"] = {
        x: newPos.x + stones["N1"].x,
        y: newPos.y + stones["N1"].y
      };
      stonePositions["N2"] = {
        x: newPos.x + stones["N2"].x,
        y: newPos.y + stones["N2"].y
      };
      return stonePositions;
    },
    SetNewPos: function(newPos) {
      this.pos = newPos;
      this.stonesFull = this.GetNewFullStonePos(newPos);
    }
  },
  // Functions
  Init: function() {
    ClearCanvas();
    this.spawn_new = true;
    for (var r = -2; r < this.ROWS; r++) {
      this.grid[r] = new Array();
      for (var c = 0; c < this.COLS; c++) {
        this.grid[r][c] = ShapeTypes.NO_SHAPE;
      }
    }
  },

  // Helpers
  CopyRow: function(oldRow, newRow) {
    for (var c = 0; c < this.COLS; ++c) {
      this.grid[newRow][c] = this.grid[oldRow][c];
    }
  },
  GetNonFullRows: function() {
    var nonFullRows = new Array();
    var curFullRow = 0;
    for (var r = this.fullRows[0]; r >= 0; --r) {
      if (
        curFullRow >= this.fullRows.length ||
        r != this.fullRows[curFullRow]
      ) {
        nonFullRows.push(r);
      } else {
        curFullRow++;
      }
    }
    return nonFullRows;
  },
  // Check Helper
  CheckInBoundsVertical: function(pos) {
    if (pos.y < -2) return false;
    if (pos.y >= this.ROWS) return false;
    return true;
  },
  CheckInBoundsHorizontal: function(pos) {
    if (pos.x < 0) return false;
    if (pos.x >= this.COLS) return false;
    return true;
  },
  CheckInBounds: function(pos) {
    if (!this.CheckInBoundsHorizontal(pos)) return false;
    if (!this.CheckInBoundsVertical(pos)) return false;
    return true;
  },
  CheckIsEmptyCell: function(pos) {
    return (
      this.grid[pos.y][pos.x] == ShapeTypes.NO_SHAPE ||
      this.grid[pos.y][pos.x] == ShapeTypes.CLEAR_RECT ||
      this.grid[pos.y][pos.x] >= 20
    );
  },

  // Update
  Update: function(dt: number) {
    if (this.Blinking.isBlinking) {
      this.Blink(dt);
      return; // Just blinking around, nothing else
    }

    this.SpawnNew();

    this.Move(dt);
    this.MoveDown(dt);

    this.CheckFullRows();
  },

  Blink: function(dt: number) {
    this.Blinking.timer -= dt;
    if (this.Blinking.timer > 0) {
      return;
    }

    this.Blinking.counter++;
    this.Blinking.timer = this.Blinking.BLINK_TIME;
    if (this.Blinking.counter == this.Blinking.BLINK_COUNT) {
      // Stop Blinking
      this.RemoveFullRows();
      this.Blinking.counter = 0;
      this.Blinking.isVisible = false;
      this.Blinking.isBlinking = false;
    }
    this.Blinking.isVisible = !this.Blinking.isVisible;
  },
  MoveDown: function(dt: number) {
    this.Moving.timerMoveDown -= dt;
    if (this.Moving.timerMoveDown > 0) {
      return;
    }
    // Move down
    var movedDown = this.CURRENT.MoveDown(this);
    this.moved = movedDown;
    this.spawn_new = !movedDown;
    if (!movedDown) {
      this.CURRENT.FixiateStones();
    }
    // Reset timer
    this.Moving.timerMoveDown = this.Moving.MOVE_DOWN_TIME;
  },
  Move: function(dt: number) {
    this.Moving.timerMove -= dt;
    if (this.Moving.timerMove > 0) {
      return;
    }
    // Move
    this.CURRENT.Move();
    this.moved = true;

    // Reset timer
    if (Input.IsDown(Keys.DOWN)) {
      this.Moving.timerMoveDown = 0;
    } // Move down when Key.DOWN is pressed
    this.Moving.timerMove = this.Moving.MOVE_TIME;
  },
  SpawnNew: function() {
    if (!this.spawn_new) {
      return;
    }

    if (!this.CURRENT.New()) {
      Tetris.Ended();
    }
    // Reset timer
    this.move_timer = this.MOVE_TIME;
    this.spawn_new = false;
  },

  CheckFullRows: function() {
    if (!this.spawn_new) {
      return;
    } // Nothing to do

    var pos = this.CURRENT.pos;
    var stones = this.CURRENT.stonesFull;
    var rows = new Array();
    for (let key in this.CURRENT.stonesFull) {
      rows[this.CURRENT.stonesFull[key].y] = true;
    }

    this.fullRows = new Array();
    for (let key in rows) {
      for (var c = 0; c < this.COLS; ++c) {
        // Row is not full
        if (this.CheckIsEmptyCell({ x: c, y: key })) {
          break;
        }
        // Rows is full
        if (c + 1 == this.COLS) {
          this.fullRows.push(parseInt(key));
        }
      }
    }
    // Start blink animation
    if (this.fullRows.length > 0) {
      this.Blinking.isBlinking = true;
    }
  },
  RemoveFullRows: function() {
    var length = this.fullRows.length;
    if (length < 1) {
      return;
    } // Nothing to do
    // Sort full rows descending
    this.fullRows.sort(function(a, b) {
      return b - a;
    });

    var nonFullRows = this.GetNonFullRows();
    var maxRow = this.fullRows[0];

    var curFullRow = 0;
    var curNonFullRow = 0;
    for (var r = maxRow; r > 0; --r) {
      if (r == this.fullRows[curFullRow]) {
        // Full row
        this.CopyRow(nonFullRows[curNonFullRow], r);
        curFullRow++;
        curNonFullRow++;
      } else {
        // non full row
        if (r > nonFullRows[curNonFullRow]) {
          this.CopyRow(nonFullRows[curNonFullRow], r);
          curNonFullRow++;
        }
      }
    }
    this.redrawRow = maxRow;
    this.fullRows = new Array();
  },

  // Drawing functions
  Draw: function() {
    if (this.Blinking.isBlinking) {
      this.DrawBlinking();
      return;
    }
    if (this.redrawRow > 0) {
      this.Redraw(this.redrawRow);
      this.redrawRow = -1;
      return;
    }
    for (var r = 0; r < this.ROWS; r++) {
      this.DrawRow(r);
    }
    //console.log("end   drawing");
  },
  DrawRow: function(r) {
    for (let c = 0; c < this.COLS; c++) {
      let shapeType = this.grid[r][c];
      if (shapeType >= 20) {
        shapeType = shapeType - 10;
      }

      if (shapeType != ShapeTypes.NO_SHAPE) {
        // We either need to clear or draw
        const pos = {
          x: c * GlobalInfo.BLOCK_WIDTH,
          y: r * GlobalInfo.BLOCK_HEIGHT
        };

        if (shapeType == ShapeTypes.CLEAR_RECT) {
          ClearRect(pos);
          this.grid[r][c] = ShapeTypes.NO_SHAPE;
        } else {
          DrawRect(pos, ShapeTypes.GetColor(shapeType));
        }
      }
    }
  },
  Redraw: function(row) {
    ClearUntilRow(row);
    for (let r = row; r >= 0; --r) {
      for (let c = 0; c < this.COLS; c++) {
        let shapeType = this.grid[r][c];
        if (shapeType >= 20) {
          shapeType = shapeType - 10;
        }

        if (
          shapeType != ShapeTypes.NO_SHAPE &&
          shapeType != ShapeTypes.CLEAR_RECT
        ) {
          // We either need to clear or draw
          const pos = {
            x: c * GlobalInfo.BLOCK_WIDTH,
            y: r * GlobalInfo.BLOCK_HEIGHT
          };
          DrawRect(pos, ShapeTypes.GetColor(shapeType));
        }
      }
    }
  },
  DrawBlinking: function() {
    if (this.Blinking.isVisible) {
      for (let key in this.fullRows) {
        this.DrawRow(this.fullRows[key]);
      }
    } else {
      for (let i = 0; i < this.fullRows.length; ++i) {
        ClearRow(this.fullRows[i]);
      }
    }
  }
};

$(document).ready(function() {
  // INPUT HANDLERS
  $(document).keyup(function(evt) {
    Input.KeyUp(evt.keyCode);
  });
  $(document).keydown(function(evt) {
    evt.preventDefault();

    if (Input.IsUp(Keys.UP) && evt.keyCode == Keys.UP) {
      Grid.CURRENT.Rotate();
    }

    if (
      (Input.IsUp(Keys.LEFT) && evt.keyCode == Keys.LEFT) ||
      (Input.IsUp(Keys.RIGHT) && evt.keyCode == Keys.RIGHT)
    ) {
      //Grid.CURRENT.Move();
      Grid.Move(Grid.Moving.MOVE_TIME);
    }
    Input.KeyDown(evt.keyCode);

    if (Input.IsDown(Keys.P)) {
      if (Tetris.State == GameStates.Paused) Tetris.Resume();
      else if (Tetris.State == GameStates.Running) Tetris.Pause();
    }
    if (Input.IsDown(Keys.R)) {
      Tetris.Start();
    }
  });

  nextStone = $(".nextStone");
  // Initialize
  Tetris.Init();
  Tetris.Start();
});

function Run(time) {
  if (Tetris.State == GameStates.Paused) {
    DrawText("Paused");
    return;
  }
  if (Tetris.State == GameStates.Ended) {
    DrawText("Game Over!");
    return;
  }

  // Game is running
  const { DT } = Update(time);
  Grid.Update(DT);
  Grid.Draw();
  requestAnimationFrame(Run);
}

function DrawRect(position: Vector2D, color) {
  // Rect
  context.fillStyle = color;
  context.fillRect(
    position.x,
    position.y,
    GlobalInfo.BLOCK_WIDTH,
    GlobalInfo.BLOCK_HEIGHT
  );
  // Specular light
  context.fillStyle = GlobalInfo.SHADOW_COLOR;
  context.fillRect(
    position.x,
    position.y,
    GlobalInfo.SHADOW_SIZE,
    GlobalInfo.SHADOW_SIZE
  );
  context.fillRect(
    position.x + GlobalInfo.SHADOW_SIZE,
    position.y + GlobalInfo.SHADOW_SIZE,
    GlobalInfo.SHADOW_SIZE,
    GlobalInfo.SHADOW_SIZE
  );
  context.fillRect(
    position.x + GlobalInfo.SHADOW_SIZE * 2,
    position.y + GlobalInfo.SHADOW_SIZE,
    GlobalInfo.SHADOW_SIZE,
    GlobalInfo.SHADOW_SIZE
  );
  context.fillRect(
    position.x + GlobalInfo.SHADOW_SIZE,
    position.y + GlobalInfo.SHADOW_SIZE * 2,
    GlobalInfo.SHADOW_SIZE,
    GlobalInfo.SHADOW_SIZE
  );
  // Shadow
  context.fillStyle = "#000";
  const strokeSize = GlobalInfo.SHADOW_SIZE;
  const y = position.y + GlobalInfo.BLOCK_HEIGHT - strokeSize;
  const x = position.x + GlobalInfo.BLOCK_WIDTH - strokeSize;
  context.fillRect(position.x, y, GlobalInfo.BLOCK_WIDTH, strokeSize);
  context.fillRect(x, position.y, strokeSize, GlobalInfo.BLOCK_HEIGHT);
}

function DrawText(text) {
  const x = canvas.width / 2;
  const y = canvas.height / 2;

  context.font = "30pt Calibri";
  // textAlign aligns text horizontally relative to placement
  context.textAlign = "center";
  // textBaseline aligns text vertically relative to font style
  context.textBaseline = "middle";
  context.fillStyle = "#FFDB00";
  context.fillText(text, x, y);
}

function ClearRect(position) {
  context.clearRect(
    position.x,
    position.y,
    GlobalInfo.BLOCK_WIDTH,
    GlobalInfo.BLOCK_HEIGHT
  );
}

function ClearCanvas() {
  context.clearRect(0, 0, GlobalInfo.CANVAS_WIDTH, GlobalInfo.CANVAS_HEIGHT);
}

function ClearUntilRow(row) {
  const height = (row + 1) * GlobalInfo.BLOCK_HEIGHT;
  context.clearRect(0, 0, GlobalInfo.CANVAS_WIDTH, height);
}

function ClearRow(row) {
  const y = row * GlobalInfo.BLOCK_HEIGHT;
  context.clearRect(0, y, GlobalInfo.CANVAS_WIDTH, GlobalInfo.BLOCK_HEIGHT);
}
