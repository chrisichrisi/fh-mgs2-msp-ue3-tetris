export enum GameStates {
  Running = 1,
  Paused = 2,
  Ended = 3
}
