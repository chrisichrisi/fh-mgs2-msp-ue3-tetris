const keys = [];

export function KeyDown(keyCode: number): void {
  keys[keyCode] = true;
}

export function KeyUp(keyCode: number): void {
  keys[keyCode] = false;
}

export function IsDown(keyCode: number): boolean {
  return keys[keyCode];
}

export function IsUp(keyCode: number): boolean {
  return typeof keys[keyCode] === "undefined" || keys[keyCode] == false;
}
