export function RandomInt(a: number, b: number): number {
  return a + Math.floor(Math.random() * (1 + b - a));
}
