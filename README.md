# Tetris

## Setup

Run `npm install` to load the required packages.

## How to use

1. Run `npm run build` to create the JS file
2. Open [index.html](./index.html) in the browser

Or open the package folder in VSCode and press `F5` to start debugging.
