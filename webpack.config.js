"use strict";

const path = require("path");

module.exports = {
  entry: "./src/App.ts",
  output: {
    filename: "tetris.js",
    path: path.resolve(__dirname, "dist")
  },

  devtool: "source-map",
  resolve: {
    extensions: [".js", ".ts"]
  },

  module: {
    rules: [
      {
        test: /\.(js|ts)$/,
        loader: "babel-loader",
        exclude: /node_modules/
      }
    ]
  }
};
